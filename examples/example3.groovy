// source: https://jenkinsci.github.io/templating-engine-plugin/v1.2/pages/Templating/index.html
// define common application environment
application_environments{
    dev{
        long_name = "Development"
        ec2{
            ips = [ "1.2.3.4", "1.2.3.5" ]
            credential_id = "ec2_ssh"
        }
        s3{
            url = "https://s3.example.com"
            path = "content/"
            credential_id = "s3_bucket"
        }
    }
}

// define pipeline libraries common between applications
libraries{
    merge = true
    sonarqube // supplies the static_code_analysis step
}