import json
import sys

# Processes the specified pipeline_config.groovy into dictionary
def convert(filename):
    # Process config file
    with open(filename, "r") as config_file:
        config = {}
        work = [config]
        # Iterate through each line in the file
        for line in config_file:
            # Strip any unnecesary characters
            line = line.strip()
            # Remove and unnecessary comments
            for i in range(0, len(line), 2):    
                if line[i:i+2] == "//":
                    if "\"" not in line or "\"" in line and i > line.rindex("\""):
                        line = line[:i].strip()
                        break
            # Check if not blank line
            if len(line) > 0:
                # Check if start of block
                if "{" in line:
                    key = line[:-1].strip()
                    tmp = {}
                    work[-1][key] = tmp
                    work.append(tmp)
                # Check if end of block
                elif "}" in line:
                    # Pop the last item from the list
                    work.pop()
                # Something else
                else:
                    # Check if key value pair
                    if "=" in line:
                        key, val = line.split("=")[0].strip(), line.split("=")[1].strip() 
                        # Strip double quotes from values
                        if "\"" in val:
                            val = val.replace("\"", "")
                        # Check if list value
                        if "[" in val or "]" in val:
                            val = [item.strip() for item in val[1:-1].split(",")]
                        # Check if boolean value
                        if val == "true":
                            val = True
                        elif val == "false":
                            val = False
                        work[-1][key] = val
                    # Check if just value and not working with root dict
                    elif len(work) > 1 and line[0].isalpha():
                        work[-1][line] = None
    # print(config)
    return config


# Saves the input dictionary config into json
def write_dict(dict_config):
    with open("converted.json", "w") as json_file:
        json.dump(dict_config, json_file)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        write_dict(convert(filename))
    else:
        print("syntax: python convert.py filename.groovy")